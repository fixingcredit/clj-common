(ns clj-common.map)

(defn path-vals
  "Transforms a (potentially nested) map into a new map with leaf values of the original map as values along with
  paths to those values in the original map (suitable for use with get-in) as keys.

  You can conceptually thing about this as flattening a nested map into a map with same leaf values of the orignal
  and keys of paths to those values in the original map.

  For example, a nested map like:

  {:a 1
   :b 2
   :c {:first  1
       :second {:nested true}}})

  Returns:

  {[:a] 1
   [:b] 2
   [:c :first] 1
   [:c :second :nested] true}"
  [v]
  (assert (map? v) "Expected input v to be a map")
  (into {}
        ((fn recursive [path v]
           (cond
             (map? v)
             (mapcat
               (fn [[k v]]
                 (recursive (conj path k) v))
               v)
             :else
             [[path v]]))
          [] v)))

(defn paths
  "Returns a sequence of paths to leaf values of a (potentially nested) map."
  [v]
  (keys (path-vals v)))
