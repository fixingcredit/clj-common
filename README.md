clj-common
==========

[![Clojars Project](https://img.shields.io/clojars/v/simplycredit/clj-common.svg)](https://clojars.org/simplycredit/clj-common)

Repository of common Clojure utilities, functions, and patterns.